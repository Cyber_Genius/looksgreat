$(document).ready(function(){

	var nanobar = new Nanobar();
	nanobar.go(100);

	$(document).on('focus', '.phone', function(event) {
        event.preventDefault();
        $(this).mask("+380 (99) 99 99 999");
    });

    $('.filter ul li a').on('click', function(event) {
    	event.preventDefault();
    	$(this).toggleClass('active');
    });



	if ($('.slider-wrapper').length>0) {
		var $status = $('.pagingInfo');
	    var $slickElement = $('.slider');
		
		$slickElement.on('init reInit afterChange', function(event, slick, currentSlide, nextSlide){
			$(this).css('visibility', 'visible');
				
			var i = (currentSlide ? currentSlide : 0) + 1;
				
			$status.html('<span class="slide" data-aos="fade-right" data-aos-duration="700">' + '0' + i + '</span>'  + '<span class="divider">/</span> ' + '<span class="slides" data-aos="fade-right" data-aos-duration="700">'+ '0' + slick.slideCount + '</span>');
		});
			
		$slickElement.slick({
			infinite:true,
			dots:false,
			prevArrow: '.slick_prev',
			nextArrow: '.slick_next',
			focusOnSelect: true,
			speed: 500,
			autoplay: false,
			autoplaySpeed: 2000,
			swipe: false,
			fade: true,
			asNavFor: '.names_slider',
			responsive: [
			    {
			      breakpoint: 992,
			      settings: {
			        slidesToShow: 1,
			        slidesToScroll: 1,
			        infinite: true,
			        dots: false,
			        fade: false,
			        swipe: true,
			      }
			    }
			]
		});

		$(".names_slider").on('init', function(event, slick) {
			$(this).css('visibility', 'visible');
		});

		$(".names_slider").slick({
			autoplay: false,
			infinite:true,
			dots:false,
			arrows:false,
			fade: true,
			asNavFor: '.slider'

		});
	}

	var clipboard = new ClipboardJS('#copy_button');

	clipboard.on('success', function(e) {
	    console.info('Action:', e.action);
	    console.info('Text:', e.text);
	    console.info('Trigger:', e.trigger);

	    e.clearSelection();
	});

	if ($('.write_text').length>0) {
		var typed = new Typed('.write_text', {
		  	strings: ["Let's make it simple.", "Let's make it simple.", "Let's make it great.", "Looksgreat."],
		  	typeSpeed: 150,
		  	backSpeed: 50,
		  	startDelay: 200,
	  		backDelay: 1500,
		  	smartBackspace: true,
		  	showCursor: false,
		  	loop: true,
	  		loopCount: Infinity,
	  		
		});
	};

	if($('.menu_button').length > 0){
        var topp = $('.menu_button').offset().top;
        var tops = $('.share_menu').offset().top - 150;
        
        $(window).scroll(function(){ 
            if(window.scrollY > topp) {
                $(".menu_button").addClass("fixed");
            };
            if(window.scrollY < topp){
                $(".menu_button").removeClass("fixed");
            };
            if(window.scrollY > tops) {
                $(".share_menu").addClass("fixed");
            };
            if(window.scrollY < tops){
                $(".share_menu").removeClass("fixed");
            };
        });
    };

    $('.menu_button').on('click',  function(event) {
    	event.preventDefault();
    	$(this).toggleClass('active');
    	$(this).siblings('.share_menu').children('.left_menu ').toggleClass('opened');
    });

    window.onscroll = function() {
	  if ($(".menu_button.fixed").hasClass('active')) {
	  	$(".menu_button.fixed").removeClass('active');
	  };
	  if ($(".share_menu.fixed div.left_menu").hasClass('opened')) {
	  	$(".share_menu.fixed div.left_menu").removeClass('opened');
	  };
	};

	$('.mobile_menu_button').on('click', function(event) {
		event.preventDefault();
		$(this).toggleClass('active').closest('.header').toggleClass('opened')
		$('body').toggleClass('fixed');
	});

    function parallax_scissors(){
        var scrolled = $(window).scrollTop();
        scrolled = scrolled - 1300;
        $('.scissors').css('top', (scrolled * 0.1) + 'px');
    }
    $(window).scroll(function(e){
        parallax_scissors();
    });

    AOS.init({
        easing: 'ease',
        duration: 500,
        disable: 'mobile',
    });

});